//
//  APIHandler.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

import Foundation
import Alamofire
import SwiftyJSON

typealias ListCallback = ([Movie]) -> Void
typealias DetailCallback = (MovieInfo) -> Void

final class APIHandler {
    private static let queue = DispatchQueue(label: "sc.movie.response-queue",
                                             qos: .userInitiated,
                                             attributes: .concurrent)

    private static let base = APIConfig.urlBase
    private static let headers: HTTPHeaders = [
        .accept("application/json")
    ]
    
    static func search(for query: String, callback: @escaping ListCallback) {
        let endPoint = "\(base)/search/movie"
        let params = [
            "api_key": APIConfig.APIKey,
            "language": NSLocale.deviceLanguage,
            "query": query
        ]
        
        AF.request(endPoint, method: .get, parameters: params, headers: headers)
            .validate()
            .responseJSON(queue: queue, options: .allowFragments, completionHandler: { response in
                guard
                    let data = response.data,
                    let json = try? JSON(data: data) else { return }

                let entries = json["results"].arrayValue
                let movies: [Movie] = entries.compactMap({ entry in
                    guard let data = try? entry.rawData() else { return nil }
                    return try? JSONDecoder().decode(Movie.self, from: data)
                })

                DispatchQueue.main.async {
                    callback(movies)
                }
            })
    }
    
    static func details(for id: Int, callback: @escaping DetailCallback) {
        let endPoint = "\(base)/movie/\(id)"
        let params = [
            "api_key": APIConfig.APIKey,
            "language": NSLocale.deviceLanguage
        ]
        
        AF.request(endPoint, method: .get, parameters: params, headers: headers)
            .validate()
            .responseJSON(queue: queue, options: .allowFragments, completionHandler: { response in
                guard
                    let data = response.data,
                    let movieInfo = try? JSONDecoder().decode(MovieInfo.self, from: data)
                else { return }
                
                DispatchQueue.main.async {
                    callback(movieInfo)
                }
            })
    }
}
