//
//  MovieListItemView.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

import UIKit

final class MovieListItemCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet private weak var cellContainer: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var posterImage: UIImageView!
    
    @IBOutlet private weak var ratingAvgLabel: UILabel!
    @IBOutlet private weak var ratingCountLabel: UILabel!
    
    @IBOutlet private weak var overviewLabel: UILabel!
    @IBOutlet private weak var overviewLeading: NSLayoutConstraint!
}

// MARK: - Lifecycle methods
extension MovieListItemCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        cellContainer.roundCorners()
        cellContainer.addShadow()
    }
}

// MARK: - Public methods
extension MovieListItemCell {
    func populate(with movie: Movie) {
        titleLabel.text = movie.title
        
        let imgPath = movie.poster_path
        if !imgPath.isEmpty {
            posterImage.loadImage(from: imgPath, callback: { [weak self] success in
                guard let `self` = self else { return }
                guard success else {
                    self.resetImage()
                    return
                }
                
                self.posterImage.roundCorners()
                self.posterImage.contentMode = .scaleAspectFill
            })
            
            overviewLeading.constant = 130.0
            overviewLabel.numberOfLines = 3
        } else {
            resetImage()
        }
        
        ratingAvgLabel.text = "\(movie.vote_average)"
        ratingCountLabel.text = "\(movie.vote_count)"
        
        overviewLabel.text = movie.overview
    }
}

// MARK: - Private methods
extension MovieListItemCell {
    func resetImage() {
        posterImage.image = UIImage(named: "placeholder")
        posterImage.layer.cornerRadius = 0.0
        posterImage.contentMode = .top
        
        overviewLeading.constant = 15.0
        overviewLabel.numberOfLines = 2
    }
}
