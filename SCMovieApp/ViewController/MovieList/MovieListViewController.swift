//
//  MovieListViewController.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

import UIKit

final class MovieListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var movieTableView: UITableView!
    @IBOutlet private weak var movieSearchField: UITextField!
    
    // MARK: - Properties
    private let cellId = "movieCell"
    private var movies: [Movie]? {
        didSet {
            movieTableView.reloadData()
        }
    }
}

// MARK: - Lifecycle methods
extension MovieListViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

// MARK: - Private methods
extension MovieListViewController {
    private func setupViews() {
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        movieTableView.register(UINib(nibName: "MovieListItemCell", bundle: nil), forCellReuseIdentifier: cellId)
        movieTableView.dataSource = self
        movieTableView.delegate = self
        
        movieSearchField.addTarget(self, action: #selector(loadMovies(_:)),
                                  for: .editingChanged)
    }
    
    @objc private func loadMovies(_ sender: UITextField) {
        guard let query = sender.text else { return }
        guard !query.isEmpty else {
            movies = nil
            return
        }
        
        APIHandler.search(for: query, callback: { [weak self] result in
            self?.movies = result
        })
    }
}

// MARK: - Datasource
extension MovieListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = movieTableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? MovieListItemCell
        else { fatalError("Failed to dequeue MovieListItemCell") }
        
        cell.selectionStyle = .none
        if let movies = self.movies {
            cell.populate(with: movies[indexPath.row])
        }
        
        return cell
    }
}

// MARK: - Delegate methods
extension MovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedMovie = movies?[indexPath.row] else { return }
        
        let detailViewController = MovieDetailViewController.fromNib
        detailViewController.movieId = selectedMovie.id
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
}
