//
//  MovieDetailViewController.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

import Foundation
import UIKit
import TagListView

final class MovieDetailViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var headerImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var voteAvgLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var popularityLabel: UILabel!
    @IBOutlet private weak var languageLabel: UILabel!
    @IBOutlet private weak var tagListView: TagListView!
    @IBOutlet private weak var overviewLabel: UILabel!
    
    // MARK: - Properties
    var movieId: Int?
    private var movieInfo: MovieInfo?
}

// MARK: - Private methods
extension MovieDetailViewController {
    private func loadData() {
        guard let id = movieId else { return }
        APIHandler.details(for: id, callback: { [weak self] movieInfo in
            self?.populate(with: movieInfo)
        })
    }
    
    private func populate(with movieInfo: MovieInfo) {
        headerImageView.loadImage(from: movieInfo.backdrop_path, fullSize: true)
        headerImageView.contentMode = .scaleAspectFill
        titleLabel.text = movieInfo.title
        voteAvgLabel.text = "\(movieInfo.vote_average)"
        statusLabel.text = movieInfo.status
        popularityLabel.text = "\(movieInfo.popularity)"
        languageLabel.text = movieInfo.original_language.uppercased()
        tagListView.addTags(movieInfo.genres.map({ $0.name }))
        overviewLabel.text = movieInfo.overview
    }
}

// MARK: - Lifecycle methods
extension MovieDetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.contentInsetAdjustmentBehavior = .never
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
}
