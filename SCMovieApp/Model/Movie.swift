//
//  Movie.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

struct Movie: Codable {
    let id: Int
    let poster_path: String
    let title: String
    let vote_average: Double
    let vote_count: Int
    let overview: String
}
