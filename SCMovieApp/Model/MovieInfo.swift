//
//  MovieInfo.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

struct Genre: Codable {
    let id: Int
    let name: String
}

struct MovieInfo: Codable {
    let id: Int
    let backdrop_path: String
    let title: String
    let vote_average: Double
    let overview: String
    let status: String
    let popularity: Double
    let original_language: String
    let genres: [Genre]
}
