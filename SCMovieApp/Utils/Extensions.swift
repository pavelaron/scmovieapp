//
//  Extensions.swift
//  SCMovieApp
//
//  Created by Pável Áron on 2021. 04. 01..
//

import UIKit
import Kingfisher

extension UIViewController {
    static var fromNib: Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }

        return instantiateFromNib()
    }
}

extension UIView {
    func roundCorners() {
        layer.cornerRadius = 10.0
    }
    
    func addShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 0.0, height: 2.5)
        layer.shadowRadius = 5.0
    }
}

extension UILabel {
    func addTrailing(image: UIImage) {
        let attachment = NSTextAttachment()
        attachment.image = image

        let attachmentString = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: text ?? "", attributes: [:])

        string.append(attachmentString)
        attributedText = string
    }
}

extension UIImageView {
    func loadImage(from path: String, callback: ((Bool) -> Void)? = nil, fullSize: Bool = false) {
        let size = fullSize ? "original" : "w200"
        let imgUrlBase = "https://image.tmdb.org/t/p/\(size)"
        guard let imgUrl = URL(string: "\(imgUrlBase)\(path)") else { return }
        
        kf.setImage(
            with: imgUrl,
            placeholder: UIImage(named: "placeholder"),
            options: [ .cacheOriginalImage ],
            completionHandler: { result in
                switch result {
                case .success:
                    callback?(true)
                case .failure:
                    callback?(false)
                }
            }
        )
    }
}

extension NSLocale {
    static var deviceLanguage: String {
        guard let currentLocale = NSLocale.current.languageCode else {
            return "en-US"
        }
        
        let supportedLangs = [
            "en": "en-US",
            "de": "de-DE",
            "it": "it-IT",
            "es": "es-ES",
            "fr": "fr-FR",
            "hu": "hu-HU"
        ]
        
        return supportedLangs[currentLocale] ?? "en-US"
    }
}
